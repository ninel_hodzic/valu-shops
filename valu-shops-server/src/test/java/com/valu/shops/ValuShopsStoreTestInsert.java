package com.valu.shops;

import com.valu.shops.models.Store;
import com.valu.shops.services.StoreService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;

@SpringBootTest()
class ValuShopsStoreTestInsert {

	@Autowired
	StoreService service;

	@BeforeEach
	public void setUp() {
		service.deleteAll();
	}

	@Test
	@Order(1)
	public void setsIdOnSave() {
		Store store = service.save(new Store("damir@"));
		Assertions.assertTrue(store.getId()!=null);
	}
}
