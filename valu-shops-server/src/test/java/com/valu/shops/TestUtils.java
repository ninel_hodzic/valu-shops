package com.valu.shops;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@TestConfiguration
public class TestUtils implements ApplicationContextAware {

    private static ApplicationContext ac;

    @Override
    public void setApplicationContext(ApplicationContext ac) {
        TestUtils.ac = ac;
    }

    public static ApplicationContext getAc() {
        return ac;
    }
}