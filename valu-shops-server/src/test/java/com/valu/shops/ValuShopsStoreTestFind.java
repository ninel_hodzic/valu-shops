package com.valu.shops;

import com.valu.shops.models.Store;
import com.valu.shops.services.StoreService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@SpringBootTest()
class ValuShopsStoreTestFind {

    @Autowired
    StoreService service;

    @BeforeEach
    public void setUp() {
        service.deleteAll();
        service.save(new Store("damir@"));
    }

    @Test
    @Order(1)
    public void getByUserId() {
        Pageable pageable = PageRequest.of(0, 1);
        Page<Store> store = service.findByUserId("damir@", pageable);
        Assertions.assertTrue(store != null);
        Assertions.assertTrue(store.getContent().size() == 1);
    }

    @Test
    @Order(2)
    public void getById() {
        //	Pageable pageable = PageRequest.of(0,1);
        List<Store> store = service.findByUserId("damir@");
        Assertions.assertTrue(store != null);
        Assertions.assertTrue(store.size() == 1);

        Store storeById = service.findById(store.get(0).getId());

        Assertions.assertTrue(store.get(0).getId().equals(storeById.getId()));
        Assertions.assertTrue(store.get(0).getUserId().equals(storeById.getUserId()));

    }

    @Test
    @Order(4)
    public void deleteById() {
        List<Store> store = service.findByUserId("damir@");
        Assertions.assertTrue(store != null);
        Assertions.assertTrue(store.size() == 1);

        service.deleteById(store.get(0).getId());

        Store storeById = service.findById(store.get(0).getId());
        Assertions.assertTrue(storeById == null);
    }
}
