package com.valu.shops.spring;

import com.valu.shops.auth.FakeUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

    private UserDetailsService userDetailsService;

    public AuthenticationTokenProcessingFilter(UserDetailsService userDetailsService){
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpRequest = this.getAsHttpRequest(servletRequest);
        String authToken = this.extractAuthTokenFromRequest(httpRequest);

        //TODO - implement this with Blockchain - e.g. integrate with lib when it is done
        UserDetails fakeUser = new FakeUser();

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                fakeUser, null, fakeUser.getAuthorities());

        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));

        SecurityContextHolder.getContext().setAuthentication(
                authentication);


        filterChain.doFilter(servletRequest, servletResponse);
    }

    private HttpServletRequest getAsHttpRequest(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            throw new RuntimeException("Expecting an HTTP request");
        }

        return (HttpServletRequest) request;
    }

    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
        /* Get token from header */
        String authToken = httpRequest.getHeader("X-Auth-Token");

        /* If token not found get it from request parameter */
        if (authToken == null) {
            authToken = httpRequest.getHeader("Authorization");
            if (null!=authToken){
                if ((authToken.toLowerCase().startsWith("Bearer"))) {
                    authToken = authToken.substring("Bearer".length()).trim();
                }
            }else {
                authToken = httpRequest.getParameter("token");
            }
        }

        return authToken;
    }
}
