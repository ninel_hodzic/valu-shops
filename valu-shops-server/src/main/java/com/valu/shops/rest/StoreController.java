package com.valu.shops.rest;

import com.valu.shops.auth.WebUser;
import com.valu.shops.models.Store;
import com.valu.shops.services.StoreService;
import com.valu.shops.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(AppConstants.API_VERSION_PREFIX + "/store")
@PreAuthorize("hasRole('USER')")
public class StoreController extends ControllerBase{

    @Autowired
    private StoreService storeService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Page<Store>> getAll(Pageable pageable) {
        WebUser user = getCurrentUser();

        Page<Store> result = storeService.findByUserId(user.getUsername(), pageable);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Store> save(@RequestBody Store item) {
        WebUser user = getCurrentUser();
        item.setUserId(user.getUsername()); // user Username or other Identifier depending on Blockchain auth
        Store result = storeService.save(item);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Store> getById(@PathVariable("id") String id) {
        Store result = storeService.findById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Store> updateById(@PathVariable("id") String id, @RequestBody Store item) {
        Store result = storeService.updateById(id, item);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Map<String,String>> deleteById(@PathVariable("id") String id) {
        storeService.deleteById(id);
        Map<String,String> m = new HashMap();
        m.put("message", "success");
        return new ResponseEntity<>(m, HttpStatus.OK);
    }
}
