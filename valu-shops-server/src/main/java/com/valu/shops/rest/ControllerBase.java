package com.valu.shops.rest;

import com.valu.shops.auth.WebUser;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.HttpClientErrorException;

public abstract class ControllerBase {

    protected WebUser getCurrentUser(){
        WebUser user = (WebUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (null==user)
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);

        return user;
    }
}
