package com.valu.shops.rest;

import com.valu.shops.utils.AppConstants;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AppConstants.API_VERSION_PREFIX + "/list")
@PreAuthorize("hasRole('USER')")
public class ListController extends ControllerBase {
}
