package com.valu.shops;

import com.valu.shops.models.Store;
import com.valu.shops.services.StoreService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication(scanBasePackages = {"com.valu"})
public class ValuShopsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ValuShopsApplication.class, args);

        // TODO - this should be removed - it is used just to have default user in app
        StoreService service = context.getBean(StoreService.class);

        service.deleteAll();
        Store store = service.save(new Store("@fake"));
        List<Store> stores = service.findByUserId("@fake");
        Store storeById = service.findById(stores.get(0).getId());
    }

}
