package com.valu.shops.services;

import com.valu.shops.models.Store;
import com.valu.shops.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class StoreService {
    @Autowired
    private StoreRepository storeRepository;

    public Page<Store> getAll(Pageable pageable) {
        return storeRepository.findAll(pageable);
    }

    public Store findById(String id) {
        Optional<Store> result = storeRepository.findById(id);
        if (result.isPresent())
            return result.get();
        else {
            return null;
        }
    }

    public Store updateById(String id, Store item) {
        boolean exists = storeRepository.existsById(id);
        if (!exists) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Store store = storeRepository.save(item);
        return store;
    }

    public void deleteById(String id) {
        storeRepository.deleteById(id);
    }

    public Store save(Store item) {
        return storeRepository.save(item);
    }

    public List<Store> findByUserId(String userId) {
        return storeRepository.findByUserId(userId);
    }

    public Page<Store> findByUserId(String userId, Pageable pageable){
        return storeRepository.findByUserId(userId, pageable);
    }

    public void deleteAll() {
        storeRepository.deleteAll();
    }

}

