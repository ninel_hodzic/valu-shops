package com.valu.shops.repositories;

import com.valu.shops.models.Store;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends MongoRepository<Store, String> {
    List<Store> findByUserId(String userId);
    Page<Store> findByUserId(String userId, Pageable pageable);
}
