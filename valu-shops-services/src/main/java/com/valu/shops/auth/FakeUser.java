package com.valu.shops.auth;

import java.util.Arrays;

/**
 * TODO - change with real user when Auth is completed
 */
@Deprecated
public class FakeUser extends WebUser{
    public FakeUser() {
        super("@fake", "@fake", getAuthorities(Arrays.asList("ROLE_USER")));
    }
}
