package com.valu.shops.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WebUser extends User {

    // here we can have custom properties and we need custom Constructor that include that properties

    public WebUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public static Collection<GrantedAuthority> getAuthorities(
            List<String> access) {
        // Create a list of grants for this user
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

        // All users are granted with ROLE_USER access
        // Therefore this user gets a ROLE_USER by default
        //authList.add(new SimpleGrantedAuthority("ROLE_USER"));

        // Check if this user has other access
        if (null != access) {
            for (String roleName : access) {
                // String roleName = role.getAuthority();

                // Add role if it doesn't exist
                if (!authList.contains(roleName)) {
                    authList.add(new SimpleGrantedAuthority(roleName));
                }
            }
        }

        // Return list of granted authorities
        return authList;
    }


}
